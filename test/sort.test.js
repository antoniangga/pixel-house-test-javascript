const expect = require("chai").expect;
const sortData = require("../lib/sort");
const dataAlphanumeric =
  "Wulan,Raharjo,Widya,10,9,102,66,5421,1,0,Yuda,Cinta,Iskandar,Hidayat,Kusuma,Indah,Jusuf";
const sortedData = [
  0,
  1,
  9,
  10,
  66,
  102,
  5421,
  "Cinta",
  "Hidayat",
  "Indah",
  "Iskandar",
  "Jusuf",
  "Kusuma",
  "Raharjo",
  "Widya",
  "Wulan",
  "Yuda",
];
describe("Sorting Data Task", () => {
  it("should return typeof ARRAY", () => {
    const result = sortData(dataAlphanumeric);
    expect(result).to.a("array");
  });
  it("should return ERROR", () => {
    const result = sortData();
    expect(result).to.equal("Error: Prameters data must be field");
  });

  it("should return sort data", () => {
    const result = sortData(dataAlphanumeric);
    expect(result[4]).to.equal(sortedData[4]);
  });
});
