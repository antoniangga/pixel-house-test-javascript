const expect = require("chai").expect;
const randomArr = require("../lib/random-array");

describe("Random Array Task", () => {
  it("should return Error", () => {
    const result = randomArr(1);
    expect(result).to.equal("ERROR: Two Parameters must be fill");
  });

  it("should return typeof ARRAY", () => {
    const result = randomArr(5, 3);
    expect(result).to.be.an("array");
  });

  it("should array length 5", () => {
    const result = randomArr(5, 3).length;
    expect(result).to.be.equal(5);
  });
});
