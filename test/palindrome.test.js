const expect = require("chai").expect;
const palindrom = require("../lib/palindrom");

describe("Palindrome Task", () => {
  it("should return Error", () => {
    const palindromText = palindrom.createPalindrom(1);
    expect(palindromText).to.equal(
      "ERROR: input polindrom must be more than 1 character"
    );
  });
  it("should return typeof STRING", () => {
    const palindromText = palindrom.createPalindrom(5);
    expect(palindromText).to.be.a("string");
  });
  it("should palindrom text", () => {
    const palindromText = palindrom.createPalindrom(5);
    const checkPalindrom = palindrom.checkPalindrom(palindromText);
    expect(checkPalindrom).to.equal(true);
  });
});
