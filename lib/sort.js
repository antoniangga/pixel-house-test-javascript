module.exports = (dataUnsort) => {
  // Using Bubble Sort
  if (!dataUnsort) {
    return "Error: Prameters data must be field";
  }
  let flagSwap;
  let data = dataUnsort.split(",");
  do {
    flagSwap = false;
    for (let i = 0; i < data.length - 1; i++) {
      isNaN(data[i]) ? (data[i] = data[i]) : (data[i] = parseInt(data[i]));
      if (data[i] > data[i + 1]) {
        let temp = data[i];
        data[i] = data[i + 1];
        data[i + 1] = temp;
        flagSwap = true;
      }
    }
  } while (flagSwap);
  return data;
};
