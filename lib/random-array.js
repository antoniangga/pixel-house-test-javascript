module.exports = (lengthArray, lengthDataText) => {
  if (!lengthDataText || !lengthArray) {
    return "ERROR: Two Parameters must be fill";
  }

  const stringArr = ",Wulan,Raharjo,Widya,Yuda,Cinta,Iskandar,Hidayat,Kusuma,Indah,Jusuf,".split(
    ","
  );
  const stringArrFilter = stringArr.filter((el) => {
    return el != "";
  });
  let result = [];

  for (j = 0; j < lengthArray; j++) {
    let textPerArray = "";
    for (i = 0; i < lengthDataText; i++) {
      const mathRandom = Math.floor(Math.random() * stringArrFilter.length);
      textPerArray += " " + stringArrFilter[mathRandom];
    }
    if (j === 0) {
      result += textPerArray.trim();
    } else {
      result += "," + textPerArray.trim();
    }
  }

  return result.split(",");
};
