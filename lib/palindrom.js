checkPalindrom = (text) => {
  let i = 0;
  let y = text.length - 1;
  while (y > i) {
    if (text[i] != text[y]) {
      return false;
    }
    i++;
    y--;
  }
  return true;
};

createPalindrom = (charLength) => {
  if (charLength >= 2) {
    const alphaNumberic =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    const numberAlphaNumberic = alphaNumberic.length;
    let result = "";
    let isPalindrom = false;

    while (isPalindrom === false) {
      for (i = 0; i < charLength; i++) {
        result += alphaNumberic.charAt(
          Math.floor(Math.random() * numberAlphaNumberic)
        );
      }
      if (!checkPalindrom(result)) {
        result = "";
        isPalindrom = false;
      } else {
        isPalindrom = true;
      }
    }

    return result;
  } else {
    return "ERROR: input polindrom must be more than 1 character";
  }
};

module.exports = {
  createPalindrom,
  checkPalindrom,
};
