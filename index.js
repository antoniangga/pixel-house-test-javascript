const palindrom = require("./lib/palindrom").createPalindrom;
const randomArr = require("./lib/random-array");
const sortData = require("./lib/sort");

const dataNumeric = "10,9,102,66,5421,1,0";
const dataString =
  "Wulan,Raharjo,Widya,Yuda,Cinta,Iskandar,Hidayat,Kusuma,Indah,Jusuf";
const dataAlphanumeric =
  "Wulan,Raharjo,Widya,10,9,102,66,5421,1,0,Yuda,Cinta,Iskandar,Hidayat,Kusuma,Indah,Jusuf";

console.log(`Task Number 1: Palindrom = ${palindrom(5)}`);
console.log(`Task Number 2: Random Array = ${randomArr(2, 3)}`);
console.log(`Task Number 3: Sort Data Numberic = ${sortData(dataNumeric)}`);
console.log(`Task Number 3: Sort Data String = ${sortData(dataString)}`);
console.log(
  `Task Number 3: Sort Data Alphanumeric = ${sortData(dataAlphanumeric)}`
);

console.log(sort(dataAlphanumeric));
